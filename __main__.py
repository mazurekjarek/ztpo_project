#! python3
# -*- coding: utf-8 -*-

from factory_method.car_model import CarModel
from factory_method.car_american_factory import AmericanCarFactory
from factory_method.car_european_factory import EuropeanCarFactory

american_factory = AmericanCarFactory()
european_factory = EuropeanCarFactory()


def make_order(order_number, factory, car_model):
    print("{0}".format("=" * 70))
    print("|{0:^68}|".format("Zamównienie nr {0: 2d}".format(order_number)))
    print("{0}".format("=" * 70))
    car = factory.order_car(car_model)
    print("{0}".format("=" * 70))
    print("|{0:^68}|".format("{0} <==> Gotowy do odbioru".format(car)))
    print("{0}".format("=" * 70))
    print("")


make_order(1, american_factory, CarModel.BMW)
make_order(2, european_factory, CarModel.Peugeot)
make_order(3, american_factory, CarModel.Audi)
make_order(4, european_factory, CarModel.BMW)
make_order(5, american_factory, CarModel.Cadillac)
make_order(6, european_factory, CarModel.Fiat)
make_order(7, american_factory, CarModel.Ford)
make_order(8, european_factory, CarModel.Renault)
