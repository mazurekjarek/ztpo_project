#__Opis Projektu__

Aplikacja służy do zbierania zamówień od klientów na produkcje wybranego modelu samochodu. Główna klasa deleguje zadanie wyprodukowania samochodu do odpowiedniej podklasy w zależności od wybranej Fabryki samochodowej.

#__Katalog projektu__

- Car Configurator Projekt
	- factory_method
		* __init__.py
		* car.py
		* car_model.py
		* car_showroom.py
		* car_european.py
		* car_american.py
		* car_european_factory.py
		* car_american_factory.py
	* __main__.py

# __Zaimplementowany wzorzec__

```
#! python3
# -*- coding: utf-8 -*-

__doc__ = "Implementacja konfiguratora samochodowego z użyciem metody wytwórczej"
```

Wzorzec Factory Method określa interfejs do tworzenia obiektów, przy czym umożliwia podklasom wyznaczenie klasy danego obiektu. Metoda wytwórcza umożliwia klasom przekazanie procesu tworzenia egzemplarzy podklasom. Aby rozszerzyć funkcjonalność Car Configuratora wystarczy utworzyć konkretna podklase samochodów która będzie dziedziczyła po głównej klasie abstrakcyjnej Car, oraz stworzyć klasy wybranych marek samochodów. Następnie należy dodać nowo stworzone samówienia samochodów do wybranej fabryki. 