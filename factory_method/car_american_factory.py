#! python3
# -*- coding: utf-8 -*-

from factory_method.car_model import  CarModel
from factory_method.car_showroom import CarShowroom
from factory_method.car_american import (BMW, Audi, Cadillac, Ford)


class AmericanCarFactory(CarShowroom):
    def __init__(self):
        self._car_list = {CarModel.BMW: BMW,
                          CarModel.Audi: Audi,
                          CarModel.Cadillac: Cadillac,
                          CarModel.Ford: Ford}

    def create_car(self, type):
        try:
            return self._car_list[type]()
        except KeyError:
            print("[AmericanCarFactory]: " + "Wybrany model samochodu \"{0}\" jest niedostępny ".format(type))
