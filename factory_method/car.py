#! python3
# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod


class Car(metaclass=ABCMeta):
    @abstractmethod
    def __init__(self):
        self._brand = "Nieznana marka"
        self._engine = "Nieznany silnik"
        self._color = "Nieznany kolor"
        self._equipment = []

    @property
    def brand(self):
        return self._brand

    def prepare(self):
        print("|{0:<68}|".format("> Przygotowanie"))
        print("|{0:<68}|".format("-> Montaż silnika"))
        print("|{0:<68}|".format("--> {0}".format(self._engine)))
        print("|{0:<68}|".format("-> Malowanie"))
        print("|{0:<68}|".format("--> {0}".format(self._color)))
        print("|{0:<68}|".format("-> Montaż wyposażenia"))

        for i in self._equipment:
            print("|{0:<68}|".format("---> {0}".format(i)))

    def traffic_lights(self):
        print("|{0:<68}|".format("> Ustawianie świateł "))

    def road_test(self):
        print("|{0:<68}|".format("> Testowanie na drodze"))

    def completing(self):
        print("|{0:<68}|".format("> Przygotowanie do przekazania klientowi"))

    def __str__(self):
        return self.brand
