#! python3
# -*- coding: utf-8 -*-

from enum import Enum, unique

@unique
class CarModel(Enum):
    BMW = "BMW"
    Audi = "Audi"
    Cadillac = "Cadillac"
    Ford = "Ford"
    Peugeot = "Peugeot"
    Fiat = "Fiat"
    Renault = "Renault"

    def __str__(self):
        return self.value
