#! python3
# -*- coding: utf-8 -*-

from abc import ABCMeta, abstractmethod


class CarShowroom(metaclass=ABCMeta):
    @abstractmethod
    def create_car(self, type):
        return NotImplemented

    def order_car(self, type):
        car = self.create_car(type)

        try:
            car.prepare()
            car.traffic_lights()
            car.road_test()
            car.completing()
        except AttributeError:
            print("[CarShowroom]: Wystąpił problem podczas realizacji zamówienia")

        return car
