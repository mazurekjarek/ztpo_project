#! python3
# -*- coding: utf-8 -*-

from factory_method.car import Car


class Peugeot(Car):
    def __init__(self):
        super().__init__()

        self._brand = "Peugeot RCZ"
        self._engine = "Silnik diesel 2.0"
        self._color = "Kolor biały"

        self._equipment.append("ParkAssistance")


class BMW(Car):
    def __init__(self):
        super().__init__()

        self._brand = "BMW E36"
        self._engine = "Silnik benzynowy 1.6"
        self._color = "Kolor rdzawy"

        self._equipment.append("Tuning agroturystyczny")


class Fiat(Car):
        def __init__(self):
            super().__init__()

            self._brand = "Fiat Multipla"
            self._engine = "Silnik benzynowy 1.5"
            self._color = "Kolor czerwony"

            self._equipment.append("Chrom pakiet")


class Renault(Car):
    def __init__(self):
        super().__init__()

        self._brand = "Renault Talisman"
        self._engine = "Silnik diesel 2.0"
        self._color = "Kolor szary"

        self._equipment.append("Klimatyzacja dwustrefowa")

