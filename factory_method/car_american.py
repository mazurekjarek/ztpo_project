#! python3
# -*- coding: utf-8 -*-

from factory_method.car import Car


class BMW(Car):
    def __init__(self):
        super().__init__()

        self._brand = "BMW X6"
        self._engine = "Silnik benzynowy 3.0"
        self._color = "Kolor czarny"

        self._equipment.append("M pakiet")

    def road_test(self):
        print("|{0:<68}|".format("> Testowanie samochodu w różnych warunkach pogodowych"))


class Audi(Car):
    def __init__(self):
        super().__init__()

        self._brand = "Audi A8"
        self._engine = "Silnik benzynowy 4.5"
        self._color = "Kolor czerwony"

        self._equipment.append("S-Line")

    def road_test(self):
        print("|{0:<68}|".format("> Testowanie samochodu w różnych warunkach pogodowych"))


class Cadillac(Car):
    def __init__(self):
        super().__init__()

        self._brand = "Cadillac Escalade"
        self._engine = "Silnik benzynowy 6.2"
        self._color = "Kolor czarny"

        self._equipment.append("Luxury pakiet")

    def road_test(self):
        print("|{0:<68}|".format("> Testowanie samochodu w różnych warunkach pogodowych"))


class Ford(Car):
    def __init__(self):
        super().__init__()

        self._brand = "Ford Explorer"
        self._engine = "Silnik benzynowy 5.0"
        self._color = "Kolor czerwony"

        self._equipment.append("Sport pakiet")


    def road_test(self):
        print("|{0:<68}|".format("> Testowanie samochodu w różnych warunkach pogodowych"))

