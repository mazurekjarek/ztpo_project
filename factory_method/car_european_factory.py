#! python3
# -*- coding: utf-8 -*-

from factory_method.car_model import  CarModel
from factory_method.car_showroom import CarShowroom
from factory_method.car_european import (Peugeot, BMW, Fiat, Renault)


class EuropeanCarFactory(CarShowroom):
    def __init__(self):
        self._car_list = {CarModel.Peugeot: Peugeot,
                          CarModel.BMW: BMW,
                          CarModel.Fiat: Fiat,
                          CarModel.Renault: Renault}

    def create_car(self, type):
        try:
            return self._car_list[type]()
        except KeyError:
            print("[EuropeanCarFactory]: " + "Wybrany model samochodu \"{0}\" jest niedostępny ".format(type))
